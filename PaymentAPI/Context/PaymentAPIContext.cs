using Microsoft.EntityFrameworkCore;
using PaymentAPI.Models;

namespace PaymentAPI.Context
{
    public class PaymentAPIContext : DbContext
    {
        public PaymentAPIContext(DbContextOptions<PaymentAPIContext> options) : base(options)
        {
            
        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedor {get; set;}
        public DbSet<ItemVenda> ItemVenda { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PaymentAPI.Models
{
    public class ItemVenda
    {
        // public ItemVenda()
        // {
        //     Venda = new Venda();
        // }
        public int? Id { get; set; }
        public string? Descricao { get; set; }
        public string? Unidade { get; set; }
        public decimal Quantidade { get; set; }
        public int VendaId { get; set; }
        public Venda Venda { get; set; }
    }
}
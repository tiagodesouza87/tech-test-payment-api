using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PaymentAPI.Models
{
    public class Venda
    {
        public Venda()
        {
            ItensVenda = new List<ItemVenda>();
        }
        public int Id { get; set; }
        public string? Descricao { get; set; }
        public EnumStatusVenda? Status { get; set; }
        public DateTime? DataVenda { get; set; }
        
        public int VendedorId { get; set; }
        public List<ItemVenda>? ItensVenda { get; set; }

    }
}
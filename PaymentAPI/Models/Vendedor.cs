using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PaymentAPI.Models
{
    public class Vendedor
    {
        public Vendedor()
        {
            Vendas = new List<Venda>();
        }
        public int Id { get; set; }
        public string? Nome { get; set; }
        public string? Cpf { get; set; }

        public List<Venda>? Vendas { get; set; }
    }
}
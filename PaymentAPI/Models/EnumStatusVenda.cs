namespace PaymentAPI.Models
{
    public enum EnumStatusVenda
    {
        AguardandoPagamento,  //Aguardando Pagamento
        EnviadoTransportadora,  //Enviado para transportadora
        Entregue,  //Entregue
        Cancelada,  //Cancelada
        PagamentoAprovado   //Pagamento aprovado
    }
}
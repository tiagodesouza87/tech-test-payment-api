using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentAPI.Models;
using PaymentAPI.Context;

namespace PaymentAPI.Controllers 
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentAPIContext _context;
        public VendaController(PaymentAPIContext context)
        {
            _context = context;
        }

        //Registro de Vendas
        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda){
            try{
                var novaVenda = new Venda();
                novaVenda.Descricao = venda.Descricao;
                novaVenda.DataVenda = venda.DataVenda;
                novaVenda.VendedorId = venda.VendedorId;
                novaVenda.Status = 0;
                if(_context.Vendedor.Find(venda.VendedorId) == null){
                    return Conflict("Vendedor não Encontrado.");
                }

                _context.Add(novaVenda);
                _context.SaveChanges();
                venda.Id = novaVenda.Id;

                foreach(var item in venda.ItensVenda){
                    var novoItem = new ItemVenda();
                    novoItem.VendaId = novaVenda.Id;
                    novoItem.Descricao = item.Descricao;
                    novoItem.Unidade = item.Unidade;
                    novoItem.Quantidade = item.Quantidade;
                    _context.Add(novoItem);
                    _context.SaveChanges();  
                    item.Id = novoItem.Id;              
                }            
                
                return CreatedAtAction(nameof(ObterVendasId), new { id = novaVenda.Id }, venda);
            }catch(Exception erro){
                 return BadRequest(new { Erro = "Houve um erro durante a execução.", Mensagem = erro.Message});
            }
        }

        //Obter venda por Id
        [HttpGet("{id}")]
        public IActionResult ObterVendasId(int id){
            try{
                var result = new Venda();
                var venda = _context.Vendas.Find(id);
                
                if(venda == null){
                    return NotFound();
                }

                result.Id = id;
                result.Descricao = venda.Descricao;
                result.DataVenda = venda.DataVenda;
                result.Status = venda.Status;
                result.VendedorId = venda.VendedorId;

                var itensVenda = _context.ItemVenda.Where(x => x.VendaId == id).ToList();

                foreach(var item in itensVenda){
                    ItemVenda itemVenda = new ItemVenda();
                    itemVenda.VendaId = id;
                    itemVenda.Id = item.Id;
                    itemVenda.Descricao = item.Descricao;
                    itemVenda.Unidade = item.Unidade;
                    itemVenda.Quantidade = item.Quantidade;

                    result.ItensVenda.Add(itemVenda);
                }
                return Ok(result);
            }catch(Exception erro){
                 return BadRequest(new { Erro = "Houve um erro durante a execução.", Mensagem = erro.Message});
            }
        }

        //Atualizar Status da venda
        [Route("AtualizarStatusVenda")]
        [HttpPut]
        public IActionResult AtualizarStatusVenda(int id, EnumStatusVenda statusVenda){
            try{
                var venda = _context.Vendas.Find(id);
                //Tratamento do Status "Aguardando Pagamento"
                if(venda.Status == EnumStatusVenda.AguardandoPagamento){
                    if(statusVenda != EnumStatusVenda.PagamentoAprovado && statusVenda != EnumStatusVenda.Cancelada){
                        return BadRequest(new { Erro = "Não é possível alterar a Venda para este status."});
                    }
                }
                //Tratamento do Status "Pagamento Aprovado"
                else if(venda.Status == EnumStatusVenda.PagamentoAprovado){
                    if(statusVenda != EnumStatusVenda.EnviadoTransportadora && statusVenda != EnumStatusVenda.Cancelada){
                        return BadRequest(new { Erro = "Não é possível alterar a Venda para este status."});
                    }
                }
                //Tratamento do Status "Enviado para Transportador"
                else if(venda.Status == EnumStatusVenda.EnviadoTransportadora){
                    if(statusVenda != EnumStatusVenda.Entregue){
                        return BadRequest(new { Erro = "Não é possível alterar a Venda para este status."});
                    }
                }else{
                    return BadRequest(new { Erro ="Não é possível alterar a Venda para este status."});
                }

                venda.Status = statusVenda;
                _context.Vendas.Update(venda);
                _context.SaveChanges();
                return Ok("Status alterado com sucesso!");
            }catch(Exception erro){
                 return BadRequest(new { Erro = "Houve um erro durante a execução.", Mensagem = erro.Message});
            }
        }

    }
}